# create_nfs_client



## Description

This role installing nfs server packets, sharing folder and tuning firewall settings

## Include role

```
- src: https://gitlab.com/ansible_roles_a_whiter/nfs/create_nfs_client.git
  version: "1.0"
  scm: git
  name: nfs_client
```


## Example playbook

```
- hosts: nfs_client
  gather_facts: true
  become: true

  roles:
    - {role: create_nfs_client, when: ansible_system == "Linux"}

```
